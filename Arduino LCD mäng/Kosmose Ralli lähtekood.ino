#include <SPFD5408_Adafruit_GFX.h>
#include <SPFD5408_Adafruit_TFTLCD.h>
#include <SPFD5408_TouchScreen.h>
#include <stdio.h>
#include <stdlib.h> 
#include <SD.h>

#define BUFFPIXEL 20
#define YP A1
#define XM A2
#define YM 5
#define XP 6 
#define TS_MINX 130 
#define TS_MINY 120 
#define TS_MAXX 920
#define TS_MAXY 920

TouchScreen ts = TouchScreen(XP, YP, XM, YM, 100);

#define LCD_CS A3
#define LCD_CD A2
#define LCD_WR A1
#define LCD_RD A0
#define LCD_RESET A4

#define SD_CS 10
#define	BLACK   0x0000
#define	BLUE    0x001F
#define	RED     0xF800
#define	GREEN   0x07E0
#define CYAN    0x07FF
#define MAGENTA 0xF81F
#define YELLOW  0xFFE0
#define WHITE   0xFFFF

Adafruit_TFTLCD tft(LCD_CS, LCD_CD, LCD_WR, LCD_RD, LCD_RESET);

#define BOXSIZE 40
#define PENRADIUS 3
#define MINPRESSURE 1
#define MAXPRESSURE 1000
#define RUUDULAIUS 40
#define PLAYERRUUDULAIUS 30
#define MOVEPIXEL 2
#define KUUBIKUIDKOKKU 6
#define PLAYERMOVEPIXEL 5

int oldGREEN, currentGREEN;

int analogPin = 5;
double button = 0;
int kontroller = 0;
File myFile;

int muutuja = 0;
void startMenu() {
  tft.fillScreen(BLACK);
  
  tft.setCursor (20, 30);
  tft.setTextSize (4);
  tft.setTextColor(GREEN);
  tft.println("Start ");
  tft.setTextSize (4.5);
  tft.setTextColor(RED);
  tft.setCursor (80, 85);
  tft.println("Game");

  tft.drawRect (20, 180, 200, 60, WHITE);
  tft.setCursor (32, 200);
  tft.setTextSize (3);
  tft.setTextColor(WHITE);
  tft.println("Scoreboard");
  
  tft.drawRect (20, 250, 200, 60, WHITE);
  tft.setCursor (65, 265);
  tft.setTextSize (4);
  tft.setTextColor(WHITE);
  tft.println("START");

  bool vajutatudStart = false;
  bool vajutatudScoreboard = false;

  TSPoint p;
  
  do {
    p= ts.getPoint(); 
  
    pinMode(XM, OUTPUT); //Pins configures again for TFT control
    pinMode(YP, OUTPUT);
    
    if (p.z > MINPRESSURE && p.z < MAXPRESSURE && p.y > 600 && p.y < 780) {
      vajutatudScoreboard = true;
    } 
    if (p.z > MINPRESSURE && p.z < MAXPRESSURE && p.y > 800 && p.y < 900){
      vajutatudStart = true;
    }
  } while(vajutatudScoreboard == false && vajutatudStart == false);

  if (vajutatudScoreboard) {
    scoreBoard();
  }
  else {
    sliderOrButton();
  }
  
}

void scoreBoard(){
  
  tft.fillScreen(BLACK);
  tft.setCursor (30, 20);
  tft.setTextSize (3);
  tft.setTextColor(GREEN);
  tft.println("Scoreboard");
  int i = 60;
  int c = 30;
  // re-open the file for reading:
  myFile = SD.open("uusScore.txt");
  tft.setTextSize (2);
  tft.setTextColor(WHITE);
  if (myFile) {
    while (myFile.available()) {
      tft.setCursor (c, i);
      char asd = char(myFile.read());
      
      if (asd == '\n'){
        i += 20;
        c = 30;
      } else {
        c += 13;
        tft.print(asd);
      }    
    }
    // close the file:
    myFile.close();
  } else {
    // if the file didn't open, print an error:
    Serial.println("error opening test.txt");
  }

  tft.drawRect (20, 250, 200, 60, WHITE);
  tft.setCursor (75, 265);
  tft.setTextSize (4);
  tft.setTextColor(WHITE);
  tft.println("Back");
  
  bool vajutatudBack = false;

  TSPoint p;
  
  do {
    p= ts.getPoint(); 
  
    pinMode(XM, OUTPUT); //Pins configures again for TFT control
    pinMode(YP, OUTPUT);
    
    if (p.z > MINPRESSURE && p.z < MAXPRESSURE && p.y > 800 && p.y < 900){
      vajutatudBack = true;
    }
  } while(vajutatudBack == false);

  if (vajutatudBack) {
    startMenu();
  }
}

void setup(void) {
  
  Serial.begin(9600);
  
  tft.reset();
  
  tft.begin(0x9341); // SDFP5408
  
  SD.begin(SD_CS);
  //Serial.println("asdasd!");
  startMenu();
  
}

void liigu (int x, int y, int color) {
    tft.fillRect(x, y + RUUDULAIUS, RUUDULAIUS, MOVEPIXEL, color);
    tft.fillRect(x, y, RUUDULAIUS, MOVEPIXEL, BLACK);
  }

void liiguP (int x, int y) {

    tft.fillRect(x + 17, y + 6, PLAYERMOVEPIXEL, 8, BLUE);
    
    tft.fillRect(x + 17, y , PLAYERMOVEPIXEL, 2, WHITE);
    tft.fillRect(x + 19, y + 2, PLAYERMOVEPIXEL, 2, WHITE);
    tft.fillRect(x + 21, y + 4, PLAYERMOVEPIXEL, 2, WHITE);
    tft.fillRect(x + 23, y + 6, PLAYERMOVEPIXEL, 2, WHITE);
    tft.fillRect(x + 25, y + 8, PLAYERMOVEPIXEL, 2, WHITE);
    tft.fillRect(x + 27, y + 10, PLAYERMOVEPIXEL, 2, WHITE);
    tft.fillRect(x + 29, y + 12, PLAYERMOVEPIXEL, 2, WHITE);
    tft.fillRect(x + 30, y + 14, PLAYERMOVEPIXEL, 5, WHITE);
    tft.fillRect(x + 22, y + 19, PLAYERMOVEPIXEL, 2, WHITE);
    tft.fillRect(x + 24, y + 21, PLAYERMOVEPIXEL, 2, WHITE);
    tft.fillRect(x + 26, y + 23, PLAYERMOVEPIXEL, 2, WHITE);
    tft.fillRect(x + 28, y + 25, PLAYERMOVEPIXEL, 2, WHITE);
    tft.fillRect(x + 30, y + 27, PLAYERMOVEPIXEL, 3, WHITE);
    tft.fillRect(x + 13, y + 6, PLAYERMOVEPIXEL, 8, WHITE);

    tft.fillRect(x + 11, y + 30, PLAYERMOVEPIXEL, 10, YELLOW);
    tft.fillRect(x + 23, y + 30, PLAYERMOVEPIXEL, 10, YELLOW);

    tft.fillRect(x + 13, y, PLAYERMOVEPIXEL, 2, BLACK);
    tft.fillRect(x + 11, y + 2, PLAYERMOVEPIXEL, 2, BLACK);
    tft.fillRect(x + 9, y + 4, PLAYERMOVEPIXEL, 2, BLACK);
    tft.fillRect(x + 7, y + 6, PLAYERMOVEPIXEL, 2, BLACK);
    tft.fillRect(x + 5, y + 8, PLAYERMOVEPIXEL, 2, BLACK);
    tft.fillRect(x + 3, y + 10, PLAYERMOVEPIXEL, 2, BLACK);
    tft.fillRect(x + 1, y + 12, PLAYERMOVEPIXEL, 2, BLACK);
    tft.fillRect(x, y + 14, PLAYERMOVEPIXEL, 5, BLACK);
    tft.fillRect(x + 8, y + 19, PLAYERMOVEPIXEL, 2, BLACK);
    tft.fillRect(x + 6, y + 21, PLAYERMOVEPIXEL, 2, BLACK);
    tft.fillRect(x + 4, y + 23, PLAYERMOVEPIXEL, 2, BLACK);
    tft.fillRect(x + 2, y + 25, PLAYERMOVEPIXEL, 2, BLACK);
    tft.fillRect(x, y + 27, PLAYERMOVEPIXEL, 3, BLACK);

    tft.fillRect(x + 7, y + 30, PLAYERMOVEPIXEL, 10, BLACK);
    tft.fillRect(x + 19, y + 30, PLAYERMOVEPIXEL, 10, BLACK);

  }



void liiguV (int x, int y) {
    tft.fillRect(x + 13 - PLAYERMOVEPIXEL, y + 6, PLAYERMOVEPIXEL, 8, BLUE);
  
    tft.fillRect(x + 13 - PLAYERMOVEPIXEL, y , PLAYERMOVEPIXEL, 2, WHITE);
    tft.fillRect(x + 11 - PLAYERMOVEPIXEL, y + 2 , PLAYERMOVEPIXEL, 2, WHITE);
    tft.fillRect(x + 9 - PLAYERMOVEPIXEL, y + 4 , PLAYERMOVEPIXEL, 2, WHITE);
    tft.fillRect(x + 7 - PLAYERMOVEPIXEL, y + 6, PLAYERMOVEPIXEL, 2, WHITE);
    tft.fillRect(x + 5 - PLAYERMOVEPIXEL, y + 8 , PLAYERMOVEPIXEL, 2, WHITE);
    tft.fillRect(x + 3 - PLAYERMOVEPIXEL, y + 10, PLAYERMOVEPIXEL, 2, WHITE);
    tft.fillRect(x + 1 - PLAYERMOVEPIXEL, y + 12, PLAYERMOVEPIXEL, 2, WHITE);
    tft.fillRect(x - PLAYERMOVEPIXEL, y + 14 , PLAYERMOVEPIXEL, 5, WHITE);
    tft.fillRect(x + 8 - PLAYERMOVEPIXEL, y + 19, PLAYERMOVEPIXEL, 2, WHITE);
    tft.fillRect(x + 6 - PLAYERMOVEPIXEL, y + 21, PLAYERMOVEPIXEL, 2, WHITE);
    tft.fillRect(x + 4 - PLAYERMOVEPIXEL, y + 23, PLAYERMOVEPIXEL, 2, WHITE);
    tft.fillRect(x + 2 - PLAYERMOVEPIXEL, y + 25, PLAYERMOVEPIXEL, 2, WHITE);
    tft.fillRect(x - PLAYERMOVEPIXEL, y + 27 , PLAYERMOVEPIXEL, 3, WHITE);
    tft.fillRect(x + 17 - PLAYERMOVEPIXEL, y + 6 , PLAYERMOVEPIXEL, 8, WHITE);
    
    tft.fillRect(x + 7 - PLAYERMOVEPIXEL, y + 30, PLAYERMOVEPIXEL, 10, YELLOW);
    tft.fillRect(x + 19 - PLAYERMOVEPIXEL, y + 30, PLAYERMOVEPIXEL, 10, YELLOW);

    tft.fillRect(x + 17 - PLAYERMOVEPIXEL, y , PLAYERMOVEPIXEL, 2, BLACK);
    tft.fillRect(x + 19 - PLAYERMOVEPIXEL, y + 2, PLAYERMOVEPIXEL, 2, BLACK);
    tft.fillRect(x + 21 - PLAYERMOVEPIXEL, y + 4, PLAYERMOVEPIXEL, 2, BLACK);
    tft.fillRect(x + 23 - PLAYERMOVEPIXEL, y + 6, PLAYERMOVEPIXEL, 2, BLACK);
    tft.fillRect(x + 25 - PLAYERMOVEPIXEL, y + 8, PLAYERMOVEPIXEL, 2, BLACK);
    tft.fillRect(x + 27 - PLAYERMOVEPIXEL, y + 10, PLAYERMOVEPIXEL, 2, BLACK);
    tft.fillRect(x + 29 - PLAYERMOVEPIXEL, y + 12, PLAYERMOVEPIXEL, 2, BLACK);
    tft.fillRect(x + 30 - PLAYERMOVEPIXEL, y + 14, PLAYERMOVEPIXEL, 5, BLACK);
    tft.fillRect(x + 22 - PLAYERMOVEPIXEL, y + 19, PLAYERMOVEPIXEL, 2, BLACK);
    tft.fillRect(x + 24 - PLAYERMOVEPIXEL, y + 21, PLAYERMOVEPIXEL, 2, BLACK);
    tft.fillRect(x + 26 - PLAYERMOVEPIXEL, y + 23, PLAYERMOVEPIXEL, 2, BLACK);
    tft.fillRect(x + 28 - PLAYERMOVEPIXEL, y + 25, PLAYERMOVEPIXEL, 2, BLACK);
    tft.fillRect(x + 30 - PLAYERMOVEPIXEL, y + 27, PLAYERMOVEPIXEL, 3, BLACK);

    tft.fillRect(x + 11 - PLAYERMOVEPIXEL, y + 30, PLAYERMOVEPIXEL, 10, BLACK);
    tft.fillRect(x + 23 - PLAYERMOVEPIXEL, y + 30, PLAYERMOVEPIXEL, 10, BLACK);
  }

boolean kasElemOnListis (int *X, int randX, int *Y, int randY) {
  int i;
  for (i = 0; i < KUUBIKUIDKOKKU; i++){
    if (X[i] == randX && Y[i] == randY) {
      return true;
    }
  }
  return false;
}

void loop() 

{
  
  srand(2); //4 tuleb wall... pseudorandom
  unsigned long start, t;
  int           x1, y1, x2, y2,
                w = tft.width(),
                h = tft.height(),
                n, i, i2,
                cx = tft.width()  / 2,
                cy = tft.height() / 2;;

  int varvid[4] = {BLUE, GREEN, YELLOW, RED};
  
  int X[KUUBIKUIDKOKKU];
  int Y[KUUBIKUIDKOKKU];
  int kuubikuVarvid[KUUBIKUIDKOKKU] = { RED, RED, RED, RED, GREEN, BLUE};
  int mituHalli = 4;
  int randX;
  int randY;
  int playerX = 105;
  int playerY = 280;
  int score = 0;

  for (i = 0; i < KUUBIKUIDKOKKU; i++) {
    do {
      randX = RUUDULAIUS + (rand() % 3) * (RUUDULAIUS + 20);
      if (i < mituHalli) randY = -RUUDULAIUS - (rand() % 3) * 4 * RUUDULAIUS;
      else randY = -RUUDULAIUS - (rand() % 6) * 2 * RUUDULAIUS;
    } while (kasElemOnListis(X, randX, Y, randY));
    X[i] = randX;
    Y[i] = randY;
  }
    
    //kuubikuVarvid[i] = varvid[rand() % 4];
  
  tft.fillScreen(BLACK);
  tft.fillRect(playerX,playerY,30,30,WHITE);
  
  tft.setCursor (205, 10);
  tft.setTextSize (1);
  tft.setTextColor(WHITE);
  tft.print(score);
  boolean gameOver = false;
  while (!gameOver){
    //TSPoint p;
    button = analogRead(analogPin);
    Serial.println(button);

    for (i = 0; i < KUUBIKUIDKOKKU; i++) {
    
      if (button != 0){
        if (((button - 31)/769)*120 + 45 < playerX - 2.5 || (button > 20 && button < 31)){
          if (muutuja == 0){
            muutuja = PLAYERMOVEPIXEL - 1;
            if (playerX >= 45){
              liiguV(playerX, playerY);
              playerX -= PLAYERMOVEPIXEL;
            }
          }
        else {
          muutuja -= 1;
          }
        }
        
        else if (((button - 31)/769)*120 + 45 > playerX + 2.5 ||(button > 1000 && button < 1010)){
            if (muutuja == 0){
              muutuja = PLAYERMOVEPIXEL - 1;
              if (playerX <= 165){
                liiguP(playerX, playerY);
                playerX += PLAYERMOVEPIXEL;
              }
            }
          else {
            muutuja -= 1;
          }
        }
    }

      if((abs(((Y[i] + RUUDULAIUS/2)) - ((playerY + PLAYERRUUDULAIUS/2))) <= ((PLAYERRUUDULAIUS + RUUDULAIUS)/2)) && (abs(((X[i] + RUUDULAIUS/2)) - ((playerX + PLAYERRUUDULAIUS/2))) <= ((PLAYERRUUDULAIUS + RUUDULAIUS)/2))){
        if (kuubikuVarvid[i] == RED){
          //game over
          tft.setCursor (60, 90);
          tft.setTextSize (5);
          tft.setTextColor(WHITE);
          tft.println("Game");
          tft.setTextSize (5);
          tft.setTextColor(WHITE);
          tft.setCursor (60, 155);
          tft.println("Over");
          waitOneTouch();
  
          gameOver = true;
        }
        if (kuubikuVarvid[i] == BLUE){
        
          score += 25;
          tft.fillRect(205, 10, 40, 40, BLACK);
          tft.setCursor (205, 10);
          tft.setTextSize (1);
          tft.setTextColor(WHITE);
          tft.print(score);
          
          tft.fillRect(X[i],Y[i],40,40,BLACK);
          Y[i] += -480;
          do {
            randX = RUUDULAIUS + (rand() % 3)*(RUUDULAIUS+20);
          }while (kasElemOnListis(X, randX,Y,Y[i]));
          X[i] = randX;
        }
        if (kuubikuVarvid[i] == GREEN){
          score += 50;
          tft.fillRect(205, 10, 40, 40, BLACK);
          tft.setCursor (205, 10);
          tft.setTextSize (1);
          tft.setTextColor(WHITE);
          tft.print(score);
          tft.fillRect(X[i],Y[i],40,40,BLACK);
          Y[i] += -480;
          do {
            randX = RUUDULAIUS + (rand() % 3)*(RUUDULAIUS+20);
          } while (kasElemOnListis(X, randX,Y,Y[i]));
          X[i] = randX;
        }
      }
      liigu(X[i], Y[i], kuubikuVarvid[i]);
      Y[i] += MOVEPIXEL;
      delay(1);
    }
    for (i = 0; i < KUUBIKUIDKOKKU; i++) {
      if (Y[i] >= w + 2*RUUDULAIUS) {
        if (i < mituHalli) {
          n = 0;
          do {
            if (n<10) randY = -4*RUUDULAIUS;
            else randY = -4*RUUDULAIUS - (rand() % 3)*4*RUUDULAIUS;
            randX = RUUDULAIUS + (rand() % 3)*(RUUDULAIUS+20);
            ++n;
          } while(kasElemOnListis(X, randX,Y,randY));
        }
        else {
          do {
            randY = -4*RUUDULAIUS - (rand() % 2)*2*RUUDULAIUS;
            randX = RUUDULAIUS + (rand() % 3)*(RUUDULAIUS+20);
          } while (kasElemOnListis(X, randX,Y,randY));
        }
      X[i] = randX;
      Y[i] = randY;
      }
    }
  }

  vahepealne(score);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

void vahepealne(int score){
   drawBorder();    ////------------------koood on lõpus
  
  // Alguse screen //////////////////////////////////////////////////////////////////////
  
  tft.setCursor (20, 30);
  tft.setTextSize (4);
  tft.setTextColor(RED);
  tft.println("Game");
  tft.setTextSize (4);
  tft.setTextColor(RED);
  tft.setCursor (80, 85);
  tft.println("Over");
  tft.setTextSize (2);
  tft.setTextColor(WHITE);
  tft.setCursor (60, 135);
  tft.print("Score: ");
  tft.print(score);
  
  tft.drawRect (20, 180, 200, 60, WHITE);
  tft.setCursor (65, 200);
  tft.setTextSize (2);
  tft.setTextColor(WHITE);
  tft.println("SAVE SCORE");
  
  tft.drawRect (20, 250, 200, 60, WHITE);
  tft.setCursor (52, 270);
  tft.setTextSize (2);
  tft.setTextColor(WHITE);
  tft.println("BACK TO MENU");

  // Wait touch ////////// kood lõpus----------------------------
  bool vajutatudScore = false;
  bool vajutatudBack = false;

  TSPoint p;
  
  do {
    p= ts.getPoint(); 
  
    pinMode(XM, OUTPUT); //Pins configures again for TFT control
    pinMode(YP, OUTPUT);
    
    if (p.z > MINPRESSURE && p.z < MAXPRESSURE && p.y > 600 && p.y < 780) {
      Serial.println("asdasd");
      vajutatudScore = true;
    } 
    if (p.z > MINPRESSURE && p.z < MAXPRESSURE && p.y > 800 && p.y < 900){
      vajutatudBack = true;
    }
  } while(vajutatudScore == false && vajutatudBack == false);

  if (vajutatudScore) {
    scorePage(score);
  }
  else {
    startMenu();
  }
 
}

void sliderOrButton(){
   drawBorder();    ////------------------koood on lõpus
  
  // Alguse screen //////////////////////////////////////////////////////////////////////
  
  tft.setCursor (20, 30);
  tft.setTextSize (4);
  tft.setTextColor(RED);
  tft.println("Vali");
  tft.setTextSize (3);
  tft.setTextColor(RED);
  tft.setCursor (40, 85);
  tft.println("Kontroller");
  
  tft.drawRect (20, 180, 200, 60, WHITE);
  tft.setCursor (90, 200);
  tft.setTextSize (2);
  tft.setTextColor(WHITE);
  tft.println("NUPUD");
  
  tft.drawRect (20, 250, 200, 60, WHITE);
  tft.setCursor (82, 270);
  tft.setTextSize (2);
  tft.setTextColor(WHITE);
  tft.println("LIUGUR");

  // Wait touch ////////// kood lõpus----------------------------
  bool vajutatudScore = false;
  bool vajutatudBack = false;

  TSPoint p;
  
  do {
    p= ts.getPoint(); 
  
    pinMode(XM, OUTPUT); //Pins configures again for TFT control
    pinMode(YP, OUTPUT);
    
    if (p.z > MINPRESSURE && p.z < MAXPRESSURE && p.y > 600 && p.y < 780) {
      Serial.println("asdasd");
      vajutatudScore = true;
    } 
    if (p.z > MINPRESSURE && p.z < MAXPRESSURE && p.y > 800 && p.y < 900){
      vajutatudBack = true;
    }
  } while(vajutatudScore == false && vajutatudBack == false);

  if (vajutatudScore) {
    kontroller = 1;
    tft.setCursor (110, 140);
    tft.setTextSize (3);
    tft.setTextColor(WHITE);
    tft.println("3..");
    delay(1000);
    tft.setCursor (110, 140);
    tft.setTextSize (3);
    tft.setTextColor(BLACK);
    tft.println("3..");
    tft.setCursor (110, 140);
    tft.setTextSize (3);
    tft.setTextColor(WHITE);
    tft.println("2..");
    delay(1000);
    tft.setCursor (110, 140);
    tft.setTextSize (3);
    tft.setTextColor(BLACK);
    tft.println("2..");
    tft.setCursor (110, 140);
    tft.setTextSize (3);
    tft.setTextColor(WHITE);
    tft.println("1..");
    delay(1000);
    tft.setCursor (110, 140);
    tft.setTextSize (3);
    tft.setTextColor(BLACK);
    tft.println("1..");
    tft.setCursor (80, 140);
    tft.setTextSize (3);
    tft.setTextColor(WHITE);
    tft.println("START!");
    delay(1000);
    loop();
  }
  else {
    kontroller = 2;
    tft.setCursor (110, 140);
    tft.setTextSize (3);
    tft.setTextColor(WHITE);
    tft.println("3..");
    delay(1000);
    tft.setCursor (110, 140);
    tft.setTextSize (3);
    tft.setTextColor(BLACK);
    tft.println("3..");
    tft.setCursor (110, 140);
    tft.setTextSize (3);
    tft.setTextColor(WHITE);
    tft.println("2..");
    delay(1000);
    tft.setCursor (110, 140);
    tft.setTextSize (3);
    tft.setTextColor(BLACK);
    tft.println("2..");
    tft.setCursor (110, 140);
    tft.setTextSize (3);
    tft.setTextColor(WHITE);
    tft.println("1..");
    delay(1000);
    tft.setCursor (110, 140);
    tft.setTextSize (3);
    tft.setTextColor(BLACK);
    tft.println("1..");
    tft.setCursor (80, 140);
    tft.setTextSize (3);
    tft.setTextColor(WHITE);
    tft.println("START!");
    delay(1000);
    loop();
  }
}

void scorePage (int score) {
  String letters = "abcdefghijklmnopqrstuvwxyz";
  
  tft.fillScreen(BLACK);
  tft.setCursor (30, 20);
  tft.setTextSize (3.9);
  tft.setTextColor(GREEN);
  tft.println("Save Score");

  tft.drawRect (20, 90, 200, 2, WHITE);
  
  tft.setTextSize (2.5);
  tft.setTextColor(WHITE);
  int h = 100;
  int w = 0;
  for (int i = 0; i < 26; i++) {
    if(i >= 13){
      h = 130;
      w = (i-13)*15;
    } else {
      w = i*15;
    }
    tft.setCursor (25 + w, h);
    tft.println(letters[i]);
  }

  tft.setCursor (60, 155);
  tft.setTextSize (2);
  tft.setTextColor(GREEN);
  tft.print("Score: ");
  tft.print(score);
  
  tft.drawRect (20, 180, 200, 60, RED);
  tft.setCursor (75, 195);
  tft.setTextSize (4);
  tft.setTextColor(RED);
  tft.println("SAVE");
  
  tft.drawRect (20, 250, 200, 60, WHITE);
  tft.setCursor (65, 265);
  tft.setTextSize (4);
  tft.setTextColor(WHITE);
  tft.println("ENTER");
  
  bool vajutatudSave = false;
  bool vajutatudEnter = false;
  String nimi = "";
  TSPoint p;
  int count = 0;
  int hhh = 100;
  int www = 0;
  int ind = 40;
  int piir = 0;
  tft.setTextSize (2.5);
  tft.setTextColor(RED);
  tft.setCursor (25, hhh);
  tft.println(letters[count]);
  
  do {

    button = analogRead(analogPin);
    tft.setTextSize (2.5);
    if (button > 900 && button < 1010){
      if (count == 0){
        tft.setTextColor(WHITE);
        tft.setCursor (25, hhh);
        tft.println(letters[count]);
        www = 1;
        count = 26;
        hhh = 130;
      }
      if (count == 13) {
        tft.setTextColor(WHITE);
        tft.setCursor (25, 130);
        tft.println(letters[count]);

        www = 0;
        hhh = 100;
      }
      count -= 1;
      tft.setTextColor(RED);
      tft.setCursor (25 + count*15 - www*(11*15+30), hhh);
      tft.println(letters[count]);
      if(count != 12){
        tft.setTextColor(WHITE);
        tft.setCursor (25 + (count + 1)*15- www*(11*15+30), hhh);
        tft.println(letters[count + 1]);
      }
      delay(200);
    }
    if (button < 600 && button > 500){
      if (count == 12){
        tft.setTextColor(WHITE);
        tft.setCursor (25 + (count)*15, hhh);
        tft.println(letters[count]);
        www = 1;
        hhh = 130;
      }
      if (count == 25) {
        tft.setTextColor(WHITE);
        tft.setCursor (25 + 12*15, hhh);
        tft.println(letters[count]);
        count = -1;
        www = 0;
        hhh = 100;
      }
      count += 1;
      tft.setTextColor(RED);
      tft.setCursor (25 + count*15 - www*(11*15+30), hhh);
      tft.println(letters[count]);
      if(count != 13){
        tft.setTextColor(WHITE);
        tft.setCursor (25 + (count - 1)*15- www*(11*15+30), hhh);
        tft.println(letters[count - 1]);
      }
      delay(200);
    }
    
    p= ts.getPoint(); 
  
    pinMode(XM, OUTPUT); //Pins configures again for TFT control
    pinMode(YP, OUTPUT);
    
    if (p.z > MINPRESSURE && p.z < MAXPRESSURE && p.y > 600 && p.y < 780) {
      if(nimi != ""){
        vajutatudSave = true;
      } 
    } 
    if (p.z > MINPRESSURE && p.z < MAXPRESSURE && p.y > 800 && p.y < 900){
      if(piir != 8){
        tft.drawRect (20, 180, 200, 60, GREEN);
        tft.setCursor (75, 195);
        tft.setTextSize (4);
        tft.setTextColor(GREEN);
        tft.println("SAVE");
        
        tft.setCursor (ind, 63);
        tft.setTextSize (3);
        tft.setTextColor(WHITE);
        tft.println(letters[count]);
        nimi += letters[count];
        delay(400);
        ind += 20;
        piir += 1;
      } else {
          tft.drawRect (20, 250, 200, 60, RED);
          tft.setCursor (65, 265);
          tft.setTextSize (4);
          tft.setTextColor(RED);
          tft.println("ENTER");
      }
    }
  } while(vajutatudSave == false && vajutatudEnter == false);

  if (vajutatudSave) {
    myFile = SD.open("uusScore.txt", FILE_WRITE);
  
    if (myFile) {
      myFile.println(nimi + ":" + score);
      myFile.close();
    } else {
      Serial.println("error opening test.txt");
    }
    startMenu();
  } 
}

TSPoint waitOneTouch() {

  
  TSPoint p;
  button = analogRead(analogPin);
  do {
    p= ts.getPoint(); 
  
    pinMode(XM, OUTPUT); //Pins configures again for TFT control
    pinMode(YP, OUTPUT);
  
  } while((p.z < MINPRESSURE )|| (p.z > MAXPRESSURE));

  return p;
}

void drawBorder () {

  // Joone joonistamine taustale

  uint16_t width = tft.width() - 1;
  uint16_t height = tft.height() - 1;
  uint8_t border = 5;

  tft.fillScreen(BLACK);    
  tft.fillRect(20, 120, 200, 2, GREEN);  
  
}